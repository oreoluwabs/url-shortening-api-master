import {
  AdvancedStatistics,
  Banner,
  ShortenSection,
  Hero,
} from "../../components/Home";

const HomePage = () => {
  return (
    <main className="text-neutral-grayishViolet">
      <Hero />
      <ShortenSection />
      <AdvancedStatistics />
      <Banner />
    </main>
  );
};

export default HomePage;
