import { useState } from "react";
import { Link } from "react-router-dom";
import ShortlyLogo from "../../assets/images/logo.svg";
import Button from "../Button";

const Navbar = () => {
  const [drawerState, setDrawerState] = useState(false);
  return (
    <header>
      <nav className="container px-4 mx-auto mt-3 h-16 flex justify-between lg:justify-start items-center relative lg:static">
        <img src={ShortlyLogo} alt="Shortly logo" />

        <div
          className="w-6 h-4 flex flex-col justify-between lg:hidden cursor-pointer"
          onClick={() => setDrawerState(!drawerState)}
        >
          <span className="w-full bg-neutral-gray h-0.5 block"></span>
          <span className="w-full bg-neutral-gray h-0.5 block"></span>
          <span className="w-full bg-neutral-gray h-0.5 block"></span>
        </div>

        <div
          className={`${
            drawerState ? "block" : "hidden"
          } lg:block absolute top-16 bg-primary-darkViolet w-11/12 mx-auto p-4 rounded-md lg:static lg:top-0 lg:bg-transparent lg:w-full lg:mx-auto lg:p-0 lg:rounded-none`}
        >
          <div className="text-white lg:text-neutral-grayishViolet font-bold lg:ml-12  lg:flex lg:items-center lg:justify-between w-full">
            <ul className="flex flex-col lg:flex-row text-center lg:text-left">
              <li className="my-4 lg:my-0 lg:mr-8">
                <Link to="/" className="hover:text-neutral-veryDarkBlue">
                  Features
                </Link>
              </li>
              <li className="my-4 lg:my-0 lg:mr-8">
                <Link to="/" className="hover:text-neutral-veryDarkBlue">
                  Pricing
                </Link>
              </li>
              <li className="my-4 lg:my-0">
                <Link to="/" className="hover:text-neutral-veryDarkBlue">
                  Resources
                </Link>
              </li>
            </ul>
            <hr className="border-0 border-b border-neutral-grayishViolet opacity-20  lg:hidden" />
            <ul className="flex flex-col lg:flex-row text-center lg:text-left items-center">
              <li className="my-4 lg:my-0 lg:mr-8">
                <Link to="/" className="hover:text-neutral-veryDarkBlue">
                  Login
                </Link>
              </li>

              <li className="my-4 lg:my-0">
                <Button rounded="rounded-full block" link to="/">
                  Sign Up
                </Button>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
};

export default Navbar;
