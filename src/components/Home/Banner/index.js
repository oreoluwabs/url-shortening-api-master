import Button from "../../Button";

const Banner = () => {
  return (
    <section className=" text-center text-white py-16 lg:py-10 bg-primary-darkViolet bg-boost-image-mobile lg:bg-boost-image-desk bg-right bg-no-repeat bg-cover">
      <h3 className="font-bold text-2xl lg:text-4xl mb-6">
        {" "}
        Boost your links today
      </h3>
      <Button>Get Started</Button>
    </section>
  );
};

export default Banner;
