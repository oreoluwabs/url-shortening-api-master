import Button from "../../Button";
import IllustrationWorking from "../../../assets/images/illustration-working.svg";

const Hero = () => {
  return (
    <section className="container sm:mt-10 mb-28 px-4 sm:mx-auto flex flex-col sm:flex-row items-center">
      <div className="sm:flex-1 sm:order-2">
        <div
        // className="relative -right-10 xl:-right-48"
        >
          <img src={IllustrationWorking} alt="working" />
        </div>
      </div>
      <div className="text-center mt-8 sm:mt-0 sm:text-left sm:flex-1  sm:order-1">
        <h1 className="font-bold text-4xl md:text-5xl xl:text-6xl text-neutral-veryDarkBlue">
          More than just shorter links
        </h1>
        <p className="my-5 lg:w-8/12 ">
          Build your brand’s recognition and get detailed insights on how your
          links are performing.
        </p>
        <Button>Get Started</Button>
      </div>
    </section>
  );
};

export default Hero;
