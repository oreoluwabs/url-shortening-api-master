import { useState } from "react";
import Button from "../../Button";

const ShortenedLink = ({ orgLink, shortLink }) => {
  const [copied, setCopied] = useState(false);

  return (
    <div className="flex flex-col sm:flex-row items-center bg-white rounded-md justify-between px-5 py-3 my-5">
      <div className="text-neutral-veryDarkBlue">
        <span>{orgLink}</span>
      </div>
      <div>
        <a href={shortLink} className="mr-6 text-primary-cyan">
          {shortLink}
        </a>
        <Button rounded="rounded-md" onClick={() => setCopied(true)}>
          {copied ? "Copied!" : "Copy"}
        </Button>
      </div>
    </div>
  );
};

const generateShortLink = (valueLink) => {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < 7; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

const ShortenSection = () => {
  const [orgLinkInput, setOrgLinkInput] = useState("");
  const [error, setError] = useState(false);
  const [data, setData] = useState([]);

  const isALink = (value) => {
    const pattern = new RegExp(
      "^(https?:\\/\\/)?" + // protocol
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
        "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
        "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
        "(\\#[-a-z\\d_]*)?$",
      "i"
    ); // fragment locator
    return pattern.test(value);
  };

  const handleShortenLink = () => {
    setError(false);
    const valueLink = orgLinkInput.trim();

    if (valueLink.length < 1 || !isALink(valueLink)) {
      setError(true);
      return;
    }
    const shortLink = generateShortLink(valueLink);
    setData([
      ...data,
      { orgLink: valueLink, shortLink: `https://rel.ink/${shortLink}` },
    ]);
  };

  return (
    <div className="bg-gray-200">
      <div className="container px-4 mx-auto pb-6 relative -top-14">
        <div className="bg-primary-darkViolet bg-shorten-image-desk bg-center-bottom rounded-md p-5 sm:p-10">
          <div className="flex flex-col sm:flex-row">
            <div className="flex-1">
              <input
                value={orgLinkInput}
                onChange={(e) => {
                  setOrgLinkInput(e.target.value);
                }}
                className="text-neutral-veryDarkBlue rounded-md w-full px-3 sm:px-6 py-1.5 focus:outline-none focus:ring-2 focus:ring-secondary-red"
                placeholder="Shorten a link..."
                type="text"
              />
            </div>
            <div className="mt-5 sm:mt-0 sm:ml-5">
              <div className="bg-white rounded-md">
                <Button rounded="rounded-md w-full" onClick={handleShortenLink}>
                  Shorten it!
                </Button>
              </div>
            </div>
          </div>
          {error && (
            <span className="absolute my-1 text-sm text-secondary-red italic">
              Please add a link
            </span>
          )}
        </div>
        <div>
          {data.map((dataItem, index) => (
            <ShortenedLink key={index + "shortendlinks"} {...dataItem} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default ShortenSection;
