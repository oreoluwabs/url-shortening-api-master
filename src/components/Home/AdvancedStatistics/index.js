import BrandRecognition from "../../../assets/images/icon-brand-recognition.svg";
import DetailedRecords from "../../../assets/images/icon-detailed-records.svg";
import FullyCustomizable from "../../../assets/images/icon-fully-customizable.svg";

const AdvancedStatistics = () => {
  return (
    <section className="bg-gray-200">
      <div className="container mx-auto text-center p-4 pt-0">
        <div>
          <h3 className="text-neutral-veryDarkBlue font-bold text-2xl lg:text-4xl">
            {" "}
            Advanced Statistics
          </h3>
          <p className="my-6 lg:w-5/12 lg:mx-auto lg:my-3">
            Track how your links are performing across the web with our advanced
            statistics dashboard.
          </p>
        </div>
        <div className="lg:text-left mt-16 lg:mt-8 mb-8 lg:mb-0 lg:flex lg:items-center relative">
          <div className="bg-primary-cyan absolute left-1/2 lg:top-1/2 transform -translate-x-1/2 w-2 h-full lg:w-full lg:h-1"></div>
          <div className="relative lg:flex my-16 lg:mr-8 lg:mt-10">
            <div className="bg-white p-6 rounded">
              <div className="absolute -top-8 left-1/2 lg:left-6 transform -translate-x-1/2 lg:-translate-x-0 ">
                <div className="bg-primary-darkViolet p-5 rounded-full">
                  <img src={BrandRecognition} alt="Brand Recognition" />
                </div>
              </div>
              <h4 className="text-xl text-neutral-veryDarkBlue font-bold py-2 pt-8">
                {" "}
                Brand Recognition
              </h4>
              <p className="pb-2">
                Boost your brand recognition with each click. Generic links
                don’t mean a thing. Branded links help instil confidence in your
                content.
              </p>
            </div>
          </div>

          <div className="relative lg:flex my-16 lg:mr-8 lg:mt-20">
            <div className="bg-white p-6 rounded">
              <div className="absolute -top-8 left-1/2 lg:left-6 transform -translate-x-1/2 lg:-translate-x-0 ">
                <div className="bg-primary-darkViolet p-5 rounded-full">
                  <img src={DetailedRecords} alt="Detailed Records" />
                </div>
              </div>
              <h4 className="text-xl text-neutral-veryDarkBlue font-bold py-2 pt-8">
                {" "}
                Detailed Records
              </h4>
              <p className="pb-2">
                Gain insights into who is clicking your links. Knowing when and
                where people engage with your content helps inform better
                decisions.
              </p>
            </div>
          </div>

          <div className="relative lg:flex mt-16 mb-0 lg:mb-16 lg:mt-32">
            <div className="bg-white p-6 rounded">
              <div className="absolute -top-8 left-1/2 lg:left-6 transform -translate-x-1/2 lg:-translate-x-0 ">
                <div className="bg-primary-darkViolet p-5 rounded-full">
                  <img src={FullyCustomizable} alt="Fully Customizable" />
                </div>
              </div>
              <h4 className="text-xl text-neutral-veryDarkBlue font-bold py-2 pt-8">
                {" "}
                Fully Customizable
              </h4>
              <p className="pb-2">
                Improve brand awareness and content discoverability through
                customizable links, supercharging audience engagement.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default AdvancedStatistics;
