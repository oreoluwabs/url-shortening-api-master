import { Link } from "react-router-dom";
import ShortlyLogo from "../../assets/images/logo.svg";
import FacebookLogo from "../../assets/images/icon-facebook.svg";
import InstagramLogo from "../../assets/images/icon-instagram.svg";
import PinterestLogo from "../../assets/images/icon-pinterest.svg";
import TwitterLogo from "../../assets/images/icon-twitter.svg";

const Footer = () => {
  return (
    <footer className="bg-neutral-veryDarkViolet p-8 text-neutral-grayishViolet">
      <div className="container mx-auto flex flex-col lg:flex-row lg:justify-between lg:w-full items-center lg:items-start text-center lg:text-left">
        <div className="my-3">
          <img src={ShortlyLogo} alt="Shortly logo" />
        </div>
        <div className="flex flex-col  lg:flex-row lg:items-start">
          <div className="text-xs lg:flex lg:mr-20">
            <div className="py-1.5 lg:pt-0 lg:px-10">
              <h5 className="text-white font-bold my-3"> Features</h5>
              <div className="my-3 hover:text-primary-cyan">
                <Link to="/">Link Shortening</Link>
              </div>
              <div className="my-3 hover:text-primary-cyan">
                <Link to="/">Branded Links</Link>
              </div>
              <div className="my-3 hover:text-primary-cyan">
                <Link to="/">Analytics</Link>
              </div>
            </div>

            <div className="py-1.5 lg:pt-0 lg:px-10">
              <h5 className="text-white font-bold my-3">Resources</h5>
              <div className="my-3 hover:text-primary-cyan">
                <Link to="/">Blog</Link>
              </div>
              <div className="my-3 hover:text-primary-cyan">
                <Link to="/">Developers</Link>
              </div>
              <div className="my-3 hover:text-primary-cyan">
                <Link to="/">Support</Link>
              </div>
            </div>

            <div className="py-1.5 lg:pt-0 lg:px-10">
              <h5 className="text-white font-bold my-3">Company</h5>
              <div className="my-3 hover:text-primary-cyan">
                <Link to="/">About</Link>
              </div>
              <div className="my-3 hover:text-primary-cyan">
                <Link to="/">Our Team</Link>
              </div>
              <div className="my-3 hover:text-primary-cyan">
                <Link to="/">Careers</Link>
              </div>
              <div className="my-3 hover:text-primary-cyan">
                <Link to="/">Contact</Link>
              </div>
            </div>
          </div>
          <div className="flex items-center my-3">
            <span className="mr-5 hover:text-primary-cyan">
              <Link to="/">
                <img src={FacebookLogo} alt="facebook account link" />
              </Link>
            </span>

            <span className="mr-5 hover:text-primary-cyan">
              <Link to="/">
                <img src={TwitterLogo} alt="twitter account link" />
              </Link>
            </span>

            <span className="mr-5 hover:text-primary-cyan">
              <Link to="/">
                <img src={PinterestLogo} alt="pinterest account link" />
              </Link>
            </span>

            <span className="mr-5 hover:text-primary-cyan">
              <Link to="/">
                <img src={InstagramLogo} alt="instagram account link" />
              </Link>
            </span>
          </div>
        </div>
      </div>

      <div className="attribution pt-8">
        Challenge by{" "}
        <a
          href="https://www.frontendmentor.io?ref=challenge"
          target="_blank"
          rel="noreferrer"
        >
          Frontend Mentor
        </a>
        . Coded by{" "}
        <a href="//" rel="noreferrer" target="_blank">
          Oreoluwa Bimbo-Salami
        </a>
        .
      </div>
    </footer>
  );
};

export default Footer;
