import { Link } from "react-router-dom";

const Button = ({ children, link, to, rounded, onClick }) => {
  const className = `bg-primary-cyan text-white px-4 py-1.5 font-normal hover:opacity-60 ${
    rounded || "rounded-full"
  } focus:bg-neutral-veryDarkBlue focus:outline-none`;

  return (
    <>
      {link && (
        <Link className={className} to={to} onClick={onClick}>
          {children}
        </Link>
      )}
      {!link && (
        <button onClick={onClick} className={className}>
          {children}
        </button>
      )}
    </>
  );
};

export default Button;
