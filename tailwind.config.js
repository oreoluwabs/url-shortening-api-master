module.exports = {
  // purge: [],
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: {
          cyan: "hsl(180, 66%, 49%)",
          darkViolet: "hsl(257, 27%, 26%)",
        },
        secondary: {
          red: "hsl(0, 87%, 67%)",
        },
        neutral: {
          gray: "hsl(0, 0%, 75%)",
          grayishViolet: "hsl(257, 7%, 63%)",
          veryDarkBlue: "hsl(255, 11%, 22%)",
          veryDarkViolet: "hsl(260, 8%, 14%)",
        },
      },

      fontFamily: {
        sans: ["Poppins", "sans-serif"],
      },

      backgroundImage: (theme) => ({
        "hero-image": "url('/src/assets/images/illustration-working.svg')",
        "shorten-image-desk":
          "url('/src/assets/images/bg-shorten-desktop.svg')",
        "shorten-image-mobile":
          "url('/src/assets/images/bg-shorten-mobile.svg')",
        "boost-image-desk": "url('/src/assets/images/bg-boost-desktop.svg')",
        "boost-image-mobile": "url('/src/assets/images/bg-boost-mobile.svg')",
      }),

      outline: {
        red: "2px solid hsl(0, 87%, 67%)",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
